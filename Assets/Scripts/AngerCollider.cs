﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngerCollider : MonoBehaviour {

    public Player_controller playerController;
    public EnemyScript enemyScript;
    public bool following;
    private GameObject player;

	// Use this for initialization
	void Start ()
    {
        playerController = GameObject.FindGameObjectWithTag("Player_tag").GetComponent<Player_controller>();
        enemyScript = GetComponentInParent<EnemyScript>();
        following = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (following)
        {
            enemyScript.timeBetweenMove = 0f;
            enemyScript.moveDirection = player.transform.position;

        }
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("anger triggered");
            playerController = other.GetComponent<Player_controller>();
            player = other.gameObject;
            following = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            following = false;
        }
    }
}
