﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player_controller : MonoBehaviour
{

    private Rigidbody2D rb2d;
    public float movementSpeed = 0;
    private Vector3 moveInput;
    private Vector3 moveVelocity;

    public int mood; // 1 = sad, 2 = calm/neutral , 3 = happy, for now, add more later
    [Range(0, 1000)]
    public float moodIndicator = 500; // Goes from 0 (death) to 1000 (super happy) ??
    public static bool staticMood;

    public CircleCollider2D intimacyCollider;
    public IntimacyCollider collScript;

    public Slider pointSlider;

    public SpriteRenderer picRenderer;
    public SpriteRenderer circleRenderer;

    public Canvas gameOverCanvas;

    public Vector3 calmScale = new Vector3(1.2f, 1.2f, 1.2f);
    public Vector3 sadScale = new Vector3(1.65f, 1.65f, 1.65f);
    public Vector3 happyScale = new Vector3(0.95f, 0.95f, 0.95f);
    public Vector3 depressedScale = new Vector3(2f, 2f, 2f);
    

    // Use this for initialization
    void Start()
    {
        intimacyCollider = GameObject.FindGameObjectWithTag("IntimacyColl").GetComponent<CircleCollider2D>();
        collScript = intimacyCollider.GetComponent<IntimacyCollider>();
        circleRenderer = collScript.gameObject.GetComponentInChildren<SpriteRenderer>();
        picRenderer = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        gameOverCanvas = GameObject.FindGameObjectWithTag("GameOver").GetComponent<Canvas>();

        Time.timeScale = 1f;
        gameOverCanvas.enabled = false;

        moodIndicator = 500;
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f);
        moveVelocity = moveInput * movementSpeed;
        pointSlider.value = moodIndicator;
        if (moodIndicator <= 0)
        {
            GAMEOVER();
        }
    }

    void FixedUpdate()
    {
        moodSet();
        variableSet(mood);
        rb2d.velocity = moveVelocity;

        if (collScript.takingDamage)
            moodIndicator -= 1f;
        else
            moodIndicator -= 0.2f;

        if (moodIndicator > 1000)
            moodIndicator = 1000;
    }

    void moodSet()
    {
        if (moodIndicator < 200f)
        {
            mood = 4; //Depressed
        }
        else if (moodIndicator < 400f)
        {
            mood = 1; // Sad
        }
        else if (moodIndicator > 700f)
        {
            mood = 3; // Happy
        }
        else
        {
            mood = 2; // Calm
        }
        if (mood == 3)      //To share this happy mood with the other scripts
            staticMood = true;
        else
            staticMood = false;

    }

    void variableSet (int currentMood)
    {
        if (currentMood == 1)
        {
            movementSpeed = 2.5f;
            intimacyCollider.transform.localScale = sadScale;
            picRenderer.color = Color.blue;
            circleRenderer.color = Color.blue;
        }
        else if (currentMood == 2)
        {
            movementSpeed = 5.5f;
            intimacyCollider.transform.localScale = calmScale;
            picRenderer.color = Color.green;
            circleRenderer.color = Color.green;
        }
        else if (currentMood == 3)
        {
            movementSpeed = 12f;
            intimacyCollider.transform.localScale = happyScale;
            picRenderer.color = Color.yellow;
            circleRenderer.color = Color.yellow;
        }
        else if (currentMood == 4)
        {
            movementSpeed = 1.5f;
            intimacyCollider.transform.localScale = depressedScale;
            picRenderer.color = new Color(0.5f, 0f, 0.5f);
            circleRenderer.color = new Color(0.5f, 0f, 0.5f);
        }
    }

    public void GAMEOVER()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        movementSpeed = 0;
        Time.timeScale = 0f;
        gameOverCanvas.enabled = true;
    }

}
