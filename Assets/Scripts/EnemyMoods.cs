﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoods : MonoBehaviour {

    public bool happy;
    public bool sad;
    public bool calm;
    public bool depressed;
    public bool angry;

    public EnemyScript enemyScript;

	// Use this for initialization
	void Start ()
    {
        enemyScript = GetComponent<EnemyScript>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (happy)
        {
            enemyScript.moveSpeed = 4f;
            enemyScript.timeBetweenMove = 1.3f;
        }
        else if (sad)
        {
            enemyScript.moveSpeed = 1f;
            enemyScript.timeBetweenMove = 2.5f;
        }
        else if (calm)
        {
            enemyScript.moveSpeed = 2f;
            enemyScript.timeBetweenMove = 2f;
        }
        else if (depressed)
        {
            enemyScript.moveSpeed = 0.4f;
            enemyScript.timeBetweenMove = 3f;
        }
        else if (angry)
        {
            enemyScript.moveSpeed = 4f;
            enemyScript.timeBetweenMove = 0.2f;
        }
    }
}
