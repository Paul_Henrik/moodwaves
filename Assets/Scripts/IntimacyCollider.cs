﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntimacyCollider : MonoBehaviour
{

    public bool takingDamage;
    private int enemyCount;

    // Use this for initialization
    void Start()
    {
        takingDamage = false;
        enemyCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyCount <= 0)
            takingDamage = false;
        else
            takingDamage = true;

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("triggered");
        if (other.tag == "Enemy")
            enemyCount++;
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Enemy")
            enemyCount--;
    }
}
