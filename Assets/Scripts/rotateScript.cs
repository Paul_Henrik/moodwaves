﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateScript : MonoBehaviour {
    public float x;
    public float y;
    public float z;
	// Update is called once per frame
	void Start ()
    {
        transform.rotation *= Quaternion.Euler(x, y, z);
    }
}
