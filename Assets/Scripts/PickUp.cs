﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {
    ParticleSystem ps;
    public static int amount;
    public AudioSource PickupSound;
    public Player_controller playerController;

    public float pointBoost;
    void Start()
    {
        amount++;
        ps = GetComponent<ParticleSystem>();
        PickupSound = GameObject.Find("PickupSound").GetComponent<AudioSource>();
        playerController = GameObject.FindGameObjectWithTag("Player_tag").GetComponent<Player_controller>();

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player_tag")
        {
            GetComponent<CircleCollider2D>().enabled = false;
            PickupSound.Play();
            ps.Stop(ps); 
            pointDistribute();
            playerController.moodIndicator += pointBoost;
            amount = 0;
            StartCoroutine("coDestroy");
        }

            
    }

    private IEnumerator coDestroy()
    {
        yield return new WaitForSeconds(2f);
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }

    void pointDistribute()
    {
        if (playerController.mood == 1)
            pointBoost = 150;
        else if (playerController.mood == 2)
            pointBoost = 100;
        else if (playerController.mood == 3)
            pointBoost = 50;
        else if (playerController.mood == 4)
            pointBoost = 200;
    }
}
