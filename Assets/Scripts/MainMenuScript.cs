﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    public Canvas buttonCanvas;
    public Canvas howToPlayCanvas;
    public AudioSource MenuClick;
    

	// Use this for initialization
	void Start ()
    {
        howToPlayCanvas.enabled = false;
        buttonCanvas.enabled = true;
        MenuClick = GameObject.Find("MenuClick").GetComponent<AudioSource>();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void startButton()
    {
        SceneManager.LoadScene(1);
        MenuClick.Play();
    }

    public void howToPlay()
    {
        buttonCanvas.enabled = false;
        howToPlayCanvas.enabled = true;
        MenuClick.Play();
    }

    public void howToPlayBackButton()
    {
        howToPlayCanvas.enabled = false;
        buttonCanvas.enabled = true;
        MenuClick.Play();
    }

    public void quitButton()
    {
        Application.Quit();
        MenuClick.Play();
    }
}
