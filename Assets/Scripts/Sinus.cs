﻿using UnityEngine;
using System.Collections;

public class Sinus : MonoBehaviour {
	Vector3 rootPos;
	public float magnitude = 5f;
	public float speed = 2f;
	[Range(0f, 3.14f)]
	public float offset = 0f;

	// Use this for initialization
	void Start () {
		rootPos = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = rootPos + new Vector3 (0, Mathf.Sin ((Time.time+offset) * speed) * magnitude, 0);
	
	}
}
