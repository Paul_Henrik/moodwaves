﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractParticles : MonoBehaviour {

    ParticleSystem[] allParticleSystems;

    public float radius = 3f;
    public float strength = 20f;
	// Use this for initialization
	void Start () {
        allParticleSystems = FindObjectsOfType<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {

        allParticleSystems = FindObjectsOfType<ParticleSystem>();
        foreach (var ps in allParticleSystems)
        {
            ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps.particleCount + 1];
            int numParticles = ps.GetParticles(particles);

            for(int i = 0; i < numParticles; i++)
            {
                ParticleSystem.Particle p = particles[i];
                Vector3 offset =  transform.position - p.position;
                Vector3 direction = offset.normalized;
                float distance = offset.magnitude;
                if(distance < radius)
                {
                    float il = Mathf.InverseLerp(radius, 0f, distance);

                    particles[i].velocity += direction * il * strength * Time.deltaTime;

                    if(distance < 0.2f)
                    {
                        particles[i].remainingLifetime = 0f;
                    }
                }
            }

            ps.SetParticles(particles, numParticles);
        }
	}
}
