﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameover_canvas : MonoBehaviour {

	public void retry()
    {
        SceneManager.LoadScene(Application.loadedLevel);
    }

    public void mainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
