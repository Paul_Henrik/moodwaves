﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float spawnTimer = 5;
    private float countDownPriv;
    private int rndSpawn;
    private int rndObj;
    private bool raiseLevel;
    public int zeppelinSpawner = 10;
    public GameObject[] pickUp;
    public GameObject[] spawnPoint;
    public GameObject[] Enemy;
//    public GameObject zeppelin;

    // Use this for initialization
    void Start()
    {
        PickUp.amount = 0;
        countDownPriv = spawnTimer;
    }

    // Update is called once per frame
    void Update()
    {
        spawnCircle();
//        spawnZeppelin();
    }

    void spawnCircle()
    {
        //spawn from a rnd place.
        rndSpawn = Random.Range(0, spawnPoint.Length);
        rndObj = Random.Range(0, Enemy.Length);

        countDownPriv -= 1f * Time.deltaTime;
        if (countDownPriv < 0)
        {
            if (PickUp.amount == 0)
                Instantiate(pickUp[0], spawnPoint[rndSpawn].transform.position, Quaternion.identity);
            else
                Instantiate(Enemy[rndObj], spawnPoint[rndSpawn].transform.position, Quaternion.identity);
            countDownPriv = spawnTimer;
        }
        if (raiseLevel == true)
        {
            Debug.Log("Raising spawnRate");
            if (spawnTimer > 0.5f)
                spawnTimer -= 0.2f;
            raiseLevel = false;
        }

    }


        /*    void spawnZeppelin()
        {
            if (GameMaster.highScore > zeppelinSpawner)
            {
                zeppelinSpawner += 1000;
                Instantiate(zeppelin, spawnPoint[5].transform.position, Quaternion.identity);
            }
        }
         */
}

