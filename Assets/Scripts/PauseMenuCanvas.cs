﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuCanvas : MonoBehaviour
{
    private Canvas pauseCanvas;

    public bool paused = false;

    public AudioSource music;
    public AudioSource MenuClick;

    void Start()
    {
        pauseCanvas = GetComponent<Canvas>();
        music = GameObject.Find("Music").GetComponent<AudioSource>();
        MenuClick = GameObject.Find("MenuClick").GetComponent<AudioSource>();
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
        }

        if (paused)
        {
            pauseCanvas.enabled = true;
            Time.timeScale = 0f;
            music.Pause();
        }
        else
        {
            pauseCanvas.enabled = false;
            Time.timeScale = 1f;
            if (!music.isPlaying)
                music.Play();
        }

    }

    public void Continue()
    {
        paused = false;
        MenuClick.Play();
    }

    public void mainMenu()
    {
        SceneManager.LoadScene(0);
        MenuClick.Play();
    }

    public void quitGame()
    {
        Application.Quit();
        MenuClick.Play();
    }

}
