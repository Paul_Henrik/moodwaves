﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayToTarget : MonoBehaviour {
    public GameObject target;
    public bool targetThePlayer;
    public bool targetIsPickUp;
    public float maxRangeToTarget;
    private Vector3 directionPickUp;
    private Ray length;
    public float rotationSpeed = 0.5f;
	
	// Update is called once per frame
	void Update ()
    {
        if (targetThePlayer)
            targetPlayer();
        if (targetIsPickUp)
            targetPickUp();
        if(target != null && targetThePlayer)
        {
            Vector2 offset = (target.transform.position - transform.position);
            Vector2 direction = offset.normalized;
            float distance = offset.magnitude;
            Debug.DrawLine(transform.position, target.transform.position);
            if (distance > maxRangeToTarget)
            {
                if (targetThePlayer)
                    Destroy(gameObject);
            }
        }



        

    }
    void targetPlayer()
    {
        if(target == null)
        {
            target = GameObject.FindObjectOfType<Player_controller>().gameObject;
        }
        else
        {

        }
    }
    
    void targetPickUp()
    {
        if (target == null)
        {
            target = GameObject.FindObjectOfType<PickUp>().gameObject;
        }
        else if (target != null)
        {
            Vector3 vectorToTarget = target.transform.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotationSpeed);
        }
        //else if (target == null)
        //    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(0f, Vector3.forward), Time.deltaTime * rotationSpeed);
            
            
    }
}
